FROM ubuntu:16.04
RUN apt-get update \
  && apt-get install -y \
    netdiag \
    traceroute \
    tcptraceroute \
    nmap \
    wget \
    curl \
    telnet \
    iputils-tracepath \
    iputils-arping \
    iputils-clockdiff \
    iputils-ping \
    jq \
    gdb \
    sysstat \
    procps \
    vim \
    git \
    findutils \
    strace \
    ltrace \
    trace-cmd \
    iftop \
    iotop \
    dstat \
    maven \
    gradle \
    tcpdump \
    mtr \
    dnsutils \
    net-tools \
    tar \
    zip \
    openssh-client \
    linux-tools-generic \
    gosu \
	&& apt-get autoremove -y \
	&& apt-get clean -y \
	&& rm -rf /var/lib/apt/lists/*


RUN git clone --depth 1 https://github.com/brendangregg/FlameGraph /root/FlameGraph
RUN git clone --depth 1 https://github.com/brendangregg/perf-tools /root/perf-tools
RUN find /root/perf-tools/** -executable -type f -exec cp {} /usr/local/bin/ \;
RUN bash -c 'echo mount -t debugfs nodev /sys/kernel/debug >> /root/.bashrc'

ADD performance-guide.txt /root/
